use axum::{
    async_trait,
    extract::{Extension, FromRequestParts, TypedHeader},
    headers::{authorization::Bearer, Authorization},
    http::request::Parts,
    RequestPartsExt,
};
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;
use uuid::Uuid;

use crate::{
    database::{self, user::User},
    error::ServerError,
};

pub struct AuthenticatedUser(pub User);

#[async_trait]
impl<S> FromRequestParts<S> for AuthenticatedUser
where
    S: Send + Sync,
{
    type Rejection = ServerError;

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Self::Rejection> {
        let Extension(connection_pool) = parts
            .extract::<Extension<Pool<SqliteConnectionManager>>>()
            .await?;
        let TypedHeader(header) = parts
            .extract::<TypedHeader<Authorization<Bearer>>>()
            .await?;
        let connection = connection_pool.get()?;
        let user_session_uuid = Uuid::try_parse(header.token())?;
        let user_session_uuid_hash = blake3::hash(user_session_uuid.as_bytes());
        let user_session =
            database::user_session::get(&connection, &user_session_uuid_hash.into())?;
        let user = database::user::get_by_uuid(&connection, &user_session.user_uuid)?;
        Ok(AuthenticatedUser(user))
    }
}
