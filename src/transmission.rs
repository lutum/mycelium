use crate::constants::{TRANSMISSION_DOWNLOADS_FOLDER_PATH, TRANSMISSION_WATCH_FOLDER_PATH};
use anyhow::Result;
use std::path::PathBuf;
use tokio::{fs::File, io::AsyncWriteExt};

pub async fn add(
    file_name: &str,
    file_bytes: &[u8],
    torrent_file_name: &str,
    torrent_bytes: &[u8],
) -> Result<()> {
    // Write file to transmission downloads folder.
    let file_path = PathBuf::from(TRANSMISSION_DOWNLOADS_FOLDER_PATH).join(&file_name);
    let mut file = File::create(&file_path).await?;
    file.write_all(file_bytes).await?;
    // Write torrent file to transmission watch folder.
    let torrent_path = PathBuf::from(TRANSMISSION_WATCH_FOLDER_PATH).join(&torrent_file_name);
    let mut torrent_file = File::create(&torrent_path).await?;
    torrent_file.write_all(torrent_bytes).await?;
    Ok(())
}
