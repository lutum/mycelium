use crate::constants::USER_SESSION_DURATION;
use crate::database::types::hash::Hash;
use crate::database::user_session::UserSession;
use crate::error::ServerError;
use crate::{database, now, ARGON2};
use argon2::PasswordVerifier;
use axum::extract::{self, Path};
use axum::http::StatusCode;
use axum::{Extension, Json};
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;
use serde::{Deserialize, Serialize};
use serde_with::{serde_as, DisplayFromStr};
use uuid::Uuid;

#[serde_as]
#[derive(Serialize)]
pub struct GetUserSessionResponse {
    pub expiration_date: u64,
    pub user_uuid: Uuid,
    #[serde_as(as = "DisplayFromStr")]
    pub uuid_hash: Hash,
}

impl From<&UserSession> for GetUserSessionResponse {
    fn from(user_session: &UserSession) -> Self {
        GetUserSessionResponse {
            expiration_date: user_session.expiration_date,
            user_uuid: user_session.user_uuid,
            uuid_hash: user_session.uuid_hash,
        }
    }
}

impl From<UserSession> for GetUserSessionResponse {
    fn from(user_session: UserSession) -> Self {
        GetUserSessionResponse {
            expiration_date: user_session.expiration_date,
            user_uuid: user_session.user_uuid,
            uuid_hash: user_session.uuid_hash,
        }
    }
}

#[derive(Deserialize)]
pub struct CreateUserSessionRequest {
    user_name: String,
    user_password: String,
}

#[derive(Serialize)]
pub struct CreateUserSessionResponse {
    user_session_expiration_date: u64,
    user_session_uuid: Uuid,
    user_uuid: Uuid,
}

pub async fn create_user_session(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    extract::Json(payload): extract::Json<CreateUserSessionRequest>,
) -> Result<(StatusCode, Json<CreateUserSessionResponse>), ServerError> {
    let connection = connection_pool.get()?;
    let user = database::user::get_by_name(&connection, &payload.user_name)?;
    let password = payload.user_password.as_bytes();
    let password_hash = user.password_hash.password_hash();
    ARGON2.verify_password(password, &password_hash)?;
    let user_session_uuid = Uuid::new_v4();
    let user_session_uuid_hash = blake3::hash(user_session_uuid.as_bytes());
    let user_session = database::user_session::UserSession {
        expiration_date: now!() + USER_SESSION_DURATION,
        user_uuid: user.uuid,
        uuid_hash: user_session_uuid_hash.into(),
    };
    database::user_session::add(&connection, &user_session)?;
    Ok((
        StatusCode::CREATED,
        Json(CreateUserSessionResponse {
            user_session_expiration_date: user_session.expiration_date,
            user_session_uuid,
            user_uuid: user.uuid,
        }),
    ))
}

pub async fn delete_user_session(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path(user_session_uuid): Path<Uuid>,
) -> Result<StatusCode, ServerError> {
    let user_session_uuid_hash = blake3::hash(user_session_uuid.as_bytes());
    let connection = connection_pool.get()?;
    database::user_session::delete(&connection, &user_session_uuid_hash.into())?;
    Ok(StatusCode::NO_CONTENT)
}

pub async fn get_user_session(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path(user_session_uuid): Path<Uuid>,
) -> Result<Json<GetUserSessionResponse>, ServerError> {
    let user_session_uuid_hash = blake3::hash(user_session_uuid.as_bytes());
    let connection = connection_pool.get()?;
    let user_session = database::user_session::get(&connection, &user_session_uuid_hash.into())?;
    Ok(Json(user_session.into()))
}

#[derive(Serialize)]
pub struct ListUserSessionsResponse(Vec<GetUserSessionResponse>);

pub async fn list_user_sessions(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path(user_uuid): Path<Uuid>,
) -> Result<Json<ListUserSessionsResponse>, ServerError> {
    let connection = connection_pool.get()?;
    let user_sessions: Vec<_> = database::user_session::get_all(&connection, &user_uuid)?
        .iter()
        .map(GetUserSessionResponse::from)
        .collect();
    Ok(Json(ListUserSessionsResponse(user_sessions)))
}
