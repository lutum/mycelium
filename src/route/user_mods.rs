use crate::constants::{
    MAX_USER_MOD_DESCRIPTION_LENGTH, MAX_USER_MOD_NAME_LENGTH, MIN_USER_MOD_NAME_LENGTH,
};
use crate::database::{self, user_mod::UserMod};
use crate::{error::ServerError, now};
use axum::extract::{self, Path};
use axum::Extension;
use axum::{http::StatusCode, Json};
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;
use serde::{Deserialize, Serialize};
use uuid::Uuid;
use validator::Validate;

#[derive(Deserialize)]
pub struct CreateUserModRequest {
    user_mod_description: Option<String>,
    user_mod_name: String,
    user_uuid: Uuid,
}

pub async fn create_user_mod(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    extract::Json(payload): extract::Json<CreateUserModRequest>,
) -> Result<(StatusCode, Json<GetUserModResponse>), ServerError> {
    let user_mod = database::user_mod::UserMod {
        creation_date: now!(),
        description: payload.user_mod_description,
        name: payload.user_mod_name,
        user_uuid: payload.user_uuid,
        uuid: Uuid::new_v4(),
        visible: true,
    };
    let connection = connection_pool.get()?;
    database::user_mod::add(&connection, &user_mod)?;
    Ok((StatusCode::CREATED, Json(user_mod.into())))
}

#[derive(Serialize)]
pub struct GetUserModResponse {
    creation_date: u64,
    description: Option<String>,
    name: String,
    user_uuid: Uuid,
    uuid: Uuid,
}

impl From<&UserMod> for GetUserModResponse {
    fn from(user_mod: &UserMod) -> Self {
        GetUserModResponse {
            creation_date: user_mod.creation_date,
            description: user_mod.description.clone(),
            name: user_mod.name.clone(),
            user_uuid: user_mod.user_uuid,
            uuid: user_mod.uuid,
        }
    }
}

impl From<UserMod> for GetUserModResponse {
    fn from(user_mod: UserMod) -> Self {
        GetUserModResponse {
            creation_date: user_mod.creation_date,
            description: user_mod.description,
            name: user_mod.name,
            user_uuid: user_mod.user_uuid,
            uuid: user_mod.uuid,
        }
    }
}

pub async fn get_user_mod(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path(user_mod_uuid): Path<Uuid>,
) -> Result<Json<GetUserModResponse>, ServerError> {
    let connection = connection_pool.get()?;
    let user_mod = database::user_mod::get(&connection, &user_mod_uuid)?;
    Ok(Json(user_mod.into()))
}

#[derive(Serialize)]
pub struct ListUserModsResponse(Vec<GetUserModResponse>);

pub async fn list_user_mods(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
) -> Result<Json<ListUserModsResponse>, ServerError> {
    let connection = connection_pool.get()?;
    let user_mods: Vec<_> = database::user_mod::get_all(&connection)?
        .iter()
        .map(GetUserModResponse::from)
        .collect();
    Ok(Json(ListUserModsResponse(user_mods)))
}

#[derive(Deserialize, Validate)]
pub struct UpdateUserModDescriptionRequest {
    #[validate(length(max = "MAX_USER_MOD_DESCRIPTION_LENGTH"))]
    description: Option<String>,
}

pub async fn update_user_mod_description(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path(user_mod_uuid): Path<Uuid>,
    extract::Json(payload): extract::Json<UpdateUserModDescriptionRequest>,
) -> Result<Json<GetUserModResponse>, ServerError> {
    let connection = connection_pool.get()?;
    database::user_mod::update_description(
        &connection,
        &user_mod_uuid,
        payload.description.as_deref(),
    )?;
    let user_mod = database::user_mod::get(&connection, &user_mod_uuid)?;
    Ok(Json(user_mod.into()))
}

#[derive(Deserialize, Validate)]
pub struct UpdateUserModNameRequest {
    #[validate(length(min = "MIN_USER_MOD_NAME_LENGTH", max = "MAX_USER_MOD_NAME_LENGTH"))]
    name: String,
}

pub async fn update_user_mod_name(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path(user_mod_uuid): Path<Uuid>,
    extract::Json(payload): extract::Json<UpdateUserModNameRequest>,
) -> Result<Json<GetUserModResponse>, ServerError> {
    let connection = connection_pool.get()?;
    database::user_mod::update_name(&connection, &user_mod_uuid, &payload.name)?;
    let user_mod = database::user_mod::get(&connection, &user_mod_uuid)?;
    Ok(Json(user_mod.into()))
}
