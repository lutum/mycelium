use anyhow::Result;
use axum::body::Body;
use axum::http::Request;
use axum::Router;
use hyper::StatusCode;
use serde_json::Value;
use tower::ServiceExt;

pub mod user_mod_images;
pub mod user_mod_releases;
pub mod user_mods;
pub mod user_sessions;
pub mod users;

#[cfg(test)]
pub mod tests;

#[cfg(debug_assertions)]
pub async fn create_user_request(user: &Value, router: &Router) -> Result<String> {
    // Create the user.
    let request = Request::post("/users")
        .header("Content-Type", "application/json")
        .body(Body::from(user.to_string()))?;
    let response = router.clone().oneshot(request).await?;
    assert_eq!(response.status(), StatusCode::CREATED);
    // Get the user's UUID.
    let bytes = hyper::body::to_bytes(response.into_body()).await?;
    let user_from_response: Value = serde_json::from_slice(&bytes)?;
    let user_uuid = user_from_response["uuid"].as_str().unwrap().to_owned();
    Ok(user_uuid)
}

#[cfg(debug_assertions)]
pub async fn create_user_mod_request(user_mod: &Value, router: &Router) -> Result<String> {
    // Create the user mod.
    let request = Request::post("/user-mods")
        .header("Content-Type", "application/json")
        .body(Body::from(user_mod.to_string()))?;
    let response = router.clone().oneshot(request).await?;
    assert_eq!(response.status(), StatusCode::CREATED);
    // Get the user mod's UUID.
    let bytes = hyper::body::to_bytes(response.into_body()).await?;
    let user_mod_from_response: Value = serde_json::from_slice(&bytes)?;
    let user_mod_uuid = user_mod_from_response["uuid"].as_str().unwrap().to_owned();
    Ok(user_mod_uuid)
}

#[cfg(debug_assertions)]
pub async fn create_user_mod_release_request(
    user_mod_uuid: &str,
    user_mod_release: &Value,
    router: &Router,
) -> Result<()> {
    // Create the user mod release.
    let url = format!("/user-mods/{}/releases", user_mod_uuid);
    let request = Request::post(url)
        .header("Content-Type", "application/json")
        .body(Body::from(user_mod_release.to_string()))?;
    let response = router.clone().oneshot(request).await?;
    assert_eq!(response.status(), StatusCode::CREATED);
    Ok(())
}
