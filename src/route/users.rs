use crate::constants::{
    MAX_USER_NAME_LENGTH, MAX_USER_PASSWORD_LENGTH, MIN_USER_NAME_LENGTH, MIN_USER_PASSWORD_LENGTH,
};
use crate::database::user::User;
use crate::error::ServerError;
use crate::{database, now, ARGON2};
use argon2::PasswordHasher;
use axum::extract::{self, Path};
use axum::Extension;
use axum::{http::StatusCode, Json};
use password_hash::SaltString;
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;
use rand::rngs::OsRng;
use serde::{Deserialize, Serialize};
use uuid::Uuid;
use validator::Validate;

#[derive(Serialize)]
pub struct GetUserResponse {
    creation_date: u64,
    name: String,
    uuid: Uuid,
}

impl From<&User> for GetUserResponse {
    fn from(user: &User) -> Self {
        GetUserResponse {
            creation_date: user.creation_date,
            name: user.name.clone(),
            uuid: user.uuid,
        }
    }
}

impl From<User> for GetUserResponse {
    fn from(user: User) -> Self {
        GetUserResponse {
            creation_date: user.creation_date,
            name: user.name,
            uuid: user.uuid,
        }
    }
}

#[derive(Deserialize, Validate)]
pub struct CreateUserRequest {
    #[validate(length(min = "MIN_USER_NAME_LENGTH", max = "MAX_USER_NAME_LENGTH"))]
    name: String,
    #[validate(length(min = "MIN_USER_PASSWORD_LENGTH", max = "MAX_USER_PASSWORD_LENGTH"))]
    password: String,
}

pub async fn create_user(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    extract::Json(payload): extract::Json<CreateUserRequest>,
) -> Result<(StatusCode, Json<GetUserResponse>), ServerError> {
    let salt = SaltString::generate(&mut OsRng);
    let password_hash = ARGON2
        .hash_password(payload.password.as_bytes(), &salt)?
        .serialize();
    let user = User {
        creation_date: now!(),
        password_hash: password_hash.into(),
        name: payload.name,
        uuid: Uuid::new_v4(),
    };
    let connection = connection_pool.get()?;
    database::user::add(&connection, &user)?;
    Ok((StatusCode::CREATED, Json(user.into())))
}

pub async fn get_user(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path(user_uuid): Path<Uuid>,
) -> Result<Json<GetUserResponse>, ServerError> {
    let connection = connection_pool.get()?;
    let user = database::user::get_by_uuid(&connection, &user_uuid)?;
    Ok(Json(user.into()))
}

#[derive(Serialize)]
pub struct ListUsersResponse(Vec<GetUserResponse>);

pub async fn list_users(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
) -> Result<Json<ListUsersResponse>, ServerError> {
    let connection = connection_pool.get()?;
    let users: Vec<_> = database::user::get_all(&connection)?
        .iter()
        .map(GetUserResponse::from)
        .collect();
    Ok(Json(ListUsersResponse(users)))
}

#[derive(Deserialize, Validate)]
pub struct UpdateUserNameRequest {
    #[validate(length(min = "MIN_USER_NAME_LENGTH", max = "MAX_USER_NAME_LENGTH"))]
    name: String,
}

pub async fn update_user_name(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path(user_uuid): Path<Uuid>,
    extract::Json(payload): extract::Json<UpdateUserNameRequest>,
) -> Result<Json<GetUserResponse>, ServerError> {
    let connection = connection_pool.get()?;
    database::user::update_name(&connection, &user_uuid, &payload.name)?;
    let user = database::user::get_by_uuid(&connection, &user_uuid)?;
    Ok(Json(user.into()))
}

#[derive(Deserialize, Validate)]
pub struct UpdateUserPasswordRequest {
    #[validate(length(min = "MIN_USER_PASSWORD_LENGTH", max = "MAX_USER_PASSWORD_LENGTH"))]
    password: String,
}

pub async fn update_user_password(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path(user_uuid): Path<Uuid>,
    extract::Json(payload): extract::Json<UpdateUserPasswordRequest>,
) -> Result<Json<GetUserResponse>, ServerError> {
    let salt = SaltString::generate(&mut OsRng);
    let password_hash = ARGON2
        .hash_password(payload.password.as_bytes(), &salt)?
        .serialize();
    let connection = connection_pool.get()?;
    database::user::update_password(&connection, &user_uuid, &password_hash.into())?;
    let user = database::user::get_by_uuid(&connection, &user_uuid)?;
    Ok(Json(user.into()))
}
