use crate::constants::IMAGES_FOLDER_PATH;
use crate::database;
use crate::database::user_mod_image::UserModImage;
use crate::error::ServerError;
use axum::{extract, Extension};
use axum::{extract::Path, Json};
use bytes::Bytes;
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use tokio::fs::File;
use tokio::io::AsyncWriteExt;
use uuid::Uuid;

#[derive(Serialize)]
pub struct GetUserModImageResponse {
    pub file_name: String,
    pub position: u64,
    pub user_mod_uuid: Uuid,
}

impl From<&UserModImage> for GetUserModImageResponse {
    fn from(user_mod_image: &UserModImage) -> Self {
        GetUserModImageResponse {
            file_name: user_mod_image.file_name.clone(),
            position: user_mod_image.position,
            user_mod_uuid: user_mod_image.user_mod_uuid,
        }
    }
}

impl From<UserModImage> for GetUserModImageResponse {
    fn from(user_mod_image: UserModImage) -> Self {
        GetUserModImageResponse {
            file_name: user_mod_image.file_name,
            position: user_mod_image.position,
            user_mod_uuid: user_mod_image.user_mod_uuid,
        }
    }
}

#[derive(Deserialize)]
pub struct CreateUserModImagesRequest {
    pub image_bytes: Bytes,
}

pub async fn create_user_mod_image(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path(user_mod_uuid): Path<Uuid>,
    extract::Json(payload): extract::Json<CreateUserModImagesRequest>,
) -> Result<Json<GetUserModImageResponse>, ServerError> {
    let uuid = Uuid::new_v4();
    let extension = image::guess_format(&payload.image_bytes)?
        .extensions_str()
        .first()
        .expect("Couldn't find an applicable extension for this format");
    let file_name = format!("{uuid}.{extension}");
    // Write image to disk.
    let image_folder = PathBuf::from(IMAGES_FOLDER_PATH).join(user_mod_uuid.to_string());
    tokio::fs::create_dir_all(&image_folder).await?;
    let image_path = image_folder.join(&file_name);
    let mut file = File::create(&image_path).await?;
    file.write_all(&payload.image_bytes).await?;
    // Add image to the database.
    let connection = connection_pool.get()?;
    let user_mod_image = database::user_mod_image::UserModImage {
        file_name,
        position: database::user_mod_image::count(&connection)?,
        user_mod_uuid,
    };
    database::user_mod_image::add(&connection, &user_mod_image)?;
    Ok(Json(user_mod_image.into()))
}

pub async fn get_user_mod_image(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path((user_mod_uuid, user_mod_image_position)): Path<(Uuid, u64)>,
) -> Result<Json<GetUserModImageResponse>, ServerError> {
    let connection = connection_pool.get()?;
    let user_mod_image =
        database::user_mod_image::get(&connection, &user_mod_uuid, user_mod_image_position)?;
    Ok(Json(user_mod_image.into()))
}

#[derive(Serialize)]
pub struct ListUserModImagesResponse(Vec<GetUserModImageResponse>);

pub async fn list_user_mod_images(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path(user_mod_uuid): Path<Uuid>,
) -> Result<Json<ListUserModImagesResponse>, ServerError> {
    let connection = connection_pool.get()?;
    let user_mod_images: Vec<_> = database::user_mod_image::get_all(&connection, &user_mod_uuid)?
        .iter()
        .map(GetUserModImageResponse::from)
        .collect();
    Ok(Json(ListUserModImagesResponse(user_mod_images)))
}
