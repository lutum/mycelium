use crate::constants::TORRENTS_FOLDER_PATH;
use crate::database::types::{hash::Hash, version::Version};
use crate::database::user_mod_release::UserModRelease;
use crate::torrent::TorrentFile;
use crate::transmission;
use crate::{database, error::ServerError, now, torrent::Torrent, tracker};
use anyhow::Result;
use axum::extract::{self, Path};
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::{Extension, Json};
use bytes::Bytes;
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;
use serde::{Deserialize, Serialize};
use serde_with::{serde_as, DisplayFromStr};
use std::path::PathBuf;
use tokio::{fs::File, io::AsyncWriteExt};
use uuid::Uuid;

#[serde_as]
#[derive(Serialize)]
pub struct GetUserModReleaseResponse {
    pub creation_date: u64,
    pub deletion_date: Option<u64>,
    pub download_count: u64,
    #[serde_as(as = "DisplayFromStr")]
    pub file_hash: Hash,
    pub file_size: u64,
    pub leecher_count: u64,
    pub magnet_uri: String,
    pub seeder_count: u64,
    pub user_mod_uuid: Uuid,
    pub version: Version,
}

impl From<&UserModRelease> for GetUserModReleaseResponse {
    fn from(user_mod_release: &UserModRelease) -> Self {
        GetUserModReleaseResponse {
            creation_date: user_mod_release.creation_date,
            deletion_date: user_mod_release.deletion_date,
            download_count: user_mod_release.download_count,
            file_hash: user_mod_release.file_hash,
            file_size: user_mod_release.file_size,
            leecher_count: user_mod_release.leecher_count,
            magnet_uri: user_mod_release.magnet_uri.clone(),
            seeder_count: user_mod_release.seeder_count,
            user_mod_uuid: user_mod_release.user_mod_uuid,
            version: user_mod_release.version.clone().into(),
        }
    }
}

impl From<UserModRelease> for GetUserModReleaseResponse {
    fn from(user_mod_release: UserModRelease) -> Self {
        GetUserModReleaseResponse {
            creation_date: user_mod_release.creation_date,
            deletion_date: user_mod_release.deletion_date,
            download_count: user_mod_release.download_count,
            file_hash: user_mod_release.file_hash,
            file_size: user_mod_release.file_size,
            leecher_count: user_mod_release.leecher_count,
            magnet_uri: user_mod_release.magnet_uri,
            seeder_count: user_mod_release.seeder_count,
            user_mod_uuid: user_mod_release.user_mod_uuid,
            version: user_mod_release.version,
        }
    }
}

#[derive(Deserialize)]
pub struct CreateUserModReleaseRequest {
    pub file_bytes: Bytes,
    pub version: Version,
}

pub async fn create_user_mod_release(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path(user_mod_uuid): Path<Uuid>,
    extract::Json(payload): extract::Json<CreateUserModReleaseRequest>,
) -> Result<(StatusCode, Json<GetUserModReleaseResponse>), ServerError> {
    let release_file_name = format!("{}-{}", user_mod_uuid, *payload.version);
    let file_name = format!("{}.zip", release_file_name);
    let torrent = Torrent::new(&file_name, &payload.file_bytes);
    let user_mod_release = database::user_mod_release::UserModRelease {
        creation_date: now!(),
        deletion_date: None,
        download_count: 0,
        file_hash: blake3::hash(&payload.file_bytes).into(),
        file_size: payload.file_bytes.len() as u64,
        leecher_count: 0,
        magnet_uri: torrent.magnet_uri(),
        seeder_count: 0,
        user_mod_uuid,
        version: payload.version,
    };
    let connection = connection_pool.get()?;
    database::user_mod_release::add(&connection, &user_mod_release)?;
    let torrent_file_name = format!("{}.torrent", release_file_name);
    let torrent_bytes = serde_bencode::to_bytes(&torrent)?;
    {
        // Write torrent file.
        let torrent_folder = PathBuf::from(TORRENTS_FOLDER_PATH).join(user_mod_uuid.to_string());
        tokio::fs::create_dir_all(&torrent_folder).await?;
        let torrent_path = torrent_folder.join(&torrent_file_name);
        let mut torrent_file = File::create(&torrent_path).await?;
        torrent_file.write_all(&torrent_bytes).await?;
    }
    tracker::add(&torrent).await?;
    transmission::add(
        &file_name,
        &payload.file_bytes,
        &torrent_file_name,
        &torrent_bytes,
    )
    .await?;
    Ok((StatusCode::CREATED, Json(user_mod_release.into())))
}

pub async fn delete_user_mod_release(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path((user_mod_uuid, user_mod_release_version)): Path<(Uuid, Version)>,
) -> Result<StatusCode, ServerError> {
    let user_mod_release_deletion_date = now!();
    let connection = connection_pool.get()?;
    database::user_mod_release::delete(
        &connection,
        &user_mod_uuid,
        user_mod_release_deletion_date,
        &user_mod_release_version,
    )?;
    Ok(StatusCode::NO_CONTENT)
}

pub async fn get_user_mod_release(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path((user_mod_uuid, user_mod_release_version)): Path<(Uuid, Version)>,
) -> Result<Json<GetUserModReleaseResponse>, ServerError> {
    let connection = connection_pool.get()?;
    let user_mod_release =
        database::user_mod_release::get(&connection, &user_mod_uuid, &user_mod_release_version)?;
    Ok(Json(user_mod_release.into()))
}

pub async fn get_user_mod_release_torrent(
    _connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path((user_mod_uuid, user_mod_release_version)): Path<(Uuid, Version)>,
) -> Result<Response, ServerError> {
    let file_name = format!("{}-{}.torrent", user_mod_uuid, *user_mod_release_version);
    let file_path = PathBuf::from(TORRENTS_FOLDER_PATH)
        .join(user_mod_uuid.to_string())
        .join(&file_name);
    let torrent_file = TorrentFile::open(&file_path).await?;
    Ok(torrent_file.into_response())
}

#[derive(Serialize)]
pub struct ListUserModReleasesResponse(Vec<GetUserModReleaseResponse>);

pub async fn list_user_mod_releases(
    connection_pool: Extension<Pool<SqliteConnectionManager>>,
    Path(user_mod_uuid): Path<Uuid>,
) -> Result<Json<ListUserModReleasesResponse>, ServerError> {
    let connection = connection_pool.get()?;
    let user_mod_releases: Vec<_> =
        database::user_mod_release::get_all(&connection, &user_mod_uuid)?
            .iter()
            .map(GetUserModReleaseResponse::from)
            .collect();
    Ok(Json(ListUserModReleasesResponse(user_mod_releases)))
}
