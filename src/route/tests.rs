use super::{create_user_mod_request, create_user_request};
use anyhow::Result;
use axum::http::Request;
use axum::{body::Body, Router};
use r2d2_sqlite::SqliteConnectionManager;
use serde_json::{json, Value};
use tower::ServiceExt;

fn router() -> Result<Router> {
    let connection_manager =
        SqliteConnectionManager::file("file:database?mode=memory&cache=shared");
    let connection_pool =
        r2d2::Pool::new(connection_manager).expect("Failed to create database connection pool");
    crate::router(connection_pool)
}

fn user_json(name: &str, password: &str) -> Value {
    json!({
        "name": name,
        "password": password,
    })
}

fn user_mod_json(name: &str, description: &str, uuid: &str) -> Value {
    json!({
        "user_mod_description": name,
        "user_mod_name": description,
        "user_uuid": uuid,
    })
}

//////////
// User //
//////////

#[tokio::test]
async fn create_user() -> Result<()> {
    let router = router()?;
    let user = user_json("AzureDiamond", "hunter2");
    create_user_request(&user, &router).await?;
    Ok(())
}

#[tokio::test]
async fn get_user() -> Result<()> {
    let router = router()?;
    let user = user_json("Cthon98", "trustno1");
    let user_uuid = create_user_request(&user, &router).await?;
    // Get the user by their UUID.
    let uri = format!("/users/{}", user_uuid);
    let request = Request::get(uri).body(Body::empty())?;
    let response = router.oneshot(request).await?;
    let bytes = hyper::body::to_bytes(response.into_body()).await?;
    let user_from_response: Value = serde_json::from_slice(&bytes)?;
    assert_eq!(user_from_response["name"], user["name"]);
    Ok(())
}

//////////////
// User mod //
//////////////

async fn get_user_mod_request(user_mod_uuid: &str, router: &Router) -> Result<Value> {
    let uri = format!("/user-mods/{}", user_mod_uuid);
    let request = Request::get(uri).body(Body::empty())?;
    let response = router.clone().oneshot(request).await?;
    let bytes = hyper::body::to_bytes(response.into_body()).await?;
    Ok(serde_json::from_slice(&bytes)?)
}

#[tokio::test]
async fn create_user_mod() -> Result<()> {
    let router = router()?;
    let user = user_json("Cras", "5Sorv6lNLKQWJuA0HZeq4AQmfgiQwx2K");
    let user_uuid = create_user_request(&user, &router).await?;
    let user_mod = user_mod_json("Suspendisse", "Ut ac.", &user_uuid);
    create_user_mod_request(&user_mod, &router).await?;
    Ok(())
}

#[tokio::test]
async fn get_user_mod() -> Result<()> {
    let router = router()?;
    let user = user_json("Duis", "A2hqxKXpHFm1Vado1u33S31GpcVSbwUZ");
    let user_uuid = create_user_request(&user, &router).await?;
    let user_mod = user_mod_json("Aliquam", "Sed elit.", &user_uuid);
    let user_mod_uuid = create_user_mod_request(&user_mod, &router).await?;
    let user_mod_from_response = get_user_mod_request(&user_mod_uuid, &router).await?;
    assert_eq!(
        user_mod_from_response["description"],
        user_mod["user_mod_description"]
    );
    assert_eq!(user_mod_from_response["name"], user_mod["user_mod_name"]);
    assert_eq!(user_mod_from_response["user_uuid"], user_mod["user_uuid"]);
    Ok(())
}

#[tokio::test]
async fn update_user_mod_description() -> Result<()> {
    unimplemented!();
}

#[tokio::test]
async fn update_user_mod_name() -> Result<()> {
    unimplemented!();
}

//////////////////////
// User mod release //
//////////////////////

#[tokio::test]
async fn create_user_mod_release() -> Result<()> {
    unimplemented!();
}

#[tokio::test]
async fn get_user_mod_release() -> Result<()> {
    unimplemented!();
}
