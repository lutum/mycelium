use super::types::hash::Hash;
use rusqlite::{named_params, Connection, Result, Row};
use uuid::Uuid;

#[derive(Debug, Eq, PartialEq)]
pub struct UserSession {
    pub expiration_date: u64,
    pub user_uuid: Uuid,
    pub uuid_hash: Hash,
}

impl TryFrom<&Row<'_>> for UserSession {
    type Error = rusqlite::Error;

    fn try_from(row: &Row) -> Result<Self, Self::Error> {
        Ok(UserSession {
            expiration_date: row.get("expiration_date")?,
            user_uuid: row.get("user_uuid")?,
            uuid_hash: row.get("uuid_hash")?,
        })
    }
}

pub fn create_table(connection: &Connection) -> Result<()> {
    connection.execute_batch(
        "CREATE TABLE IF NOT EXISTS UserSession(
                   uuid_hash  BLOB   NOT NULL PRIMARY KEY,
                   user_uuid  BLOB   NOT NULL,
             expiration_date INTEGER NOT NULL,
             FOREIGN KEY(user_uuid) REFERENCES User(uuid)
         ) STRICT",
    )
}

pub fn add(connection: &Connection, user_session: &UserSession) -> Result<usize> {
    connection.execute(
        "INSERT INTO UserSession(expiration_date, user_uuid, uuid_hash)
         VALUES(:expiration_date, :user_uuid, :uuid_hash)",
        named_params! {
            ":expiration_date": user_session.expiration_date,
            ":user_uuid": user_session.user_uuid,
            ":uuid_hash": user_session.uuid_hash,
        },
    )
}

pub fn delete(connection: &Connection, user_session_uuid_hash: &Hash) -> Result<usize> {
    connection.execute(
        "DELETE FROM UserSession WHERE uuid_hash = :uuid_hash",
        named_params! { ":uuid_hash": user_session_uuid_hash, },
    )
}

pub fn get(connection: &Connection, user_session_uuid_hash: &Hash) -> Result<UserSession> {
    connection.query_row(
        "SELECT expiration_date, user_uuid, uuid_hash
           FROM UserSession
          WHERE uuid_hash = :uuid_hash",
        named_params! { ":uuid_hash": user_session_uuid_hash },
        |row| row.try_into(),
    )
}

pub fn get_all(connection: &Connection, user_uuid: &Uuid) -> Result<Vec<UserSession>> {
    connection
        .prepare(
            "SELECT expiration_date, user_uuid, uuid_hash
               FROM UserSession
              WHERE user_uuid = :user_uuid",
        )?
        .query_map(named_params! { ":user_uuid": user_uuid }, |row| {
            row.try_into()
        })?
        .collect()
}
