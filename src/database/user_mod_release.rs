use super::types::hash::Hash;
use super::types::version::Version;
use rusqlite::{named_params, Connection, Result, Row};
use uuid::Uuid;

pub struct UserModRelease {
    pub creation_date: u64,
    pub deletion_date: Option<u64>,
    pub download_count: u64,
    pub file_hash: Hash,
    pub file_size: u64,
    pub leecher_count: u64,
    pub magnet_uri: String,
    pub seeder_count: u64,
    pub user_mod_uuid: Uuid,
    pub version: Version,
}

impl TryFrom<&Row<'_>> for UserModRelease {
    type Error = rusqlite::Error;

    fn try_from(row: &Row) -> Result<Self, Self::Error> {
        Ok(UserModRelease {
            creation_date: row.get("creation_date")?,
            deletion_date: row.get("deletion_date")?,
            download_count: row.get("download_count")?,
            file_hash: row.get("file_hash")?,
            file_size: row.get("file_size")?,
            leecher_count: row.get("leecher_count")?,
            magnet_uri: row.get("magnet_uri")?,
            seeder_count: row.get("seeder_count")?,
            user_mod_uuid: row.get("user_mod_uuid")?,
            version: row.get("version")?,
        })
    }
}

pub fn create_table(connection: &Connection) -> Result<()> {
    connection.execute_batch(
        "CREATE TABLE IF NOT EXISTS UserModRelease(
                       version  TEXT   NOT NULL,
                     file_hash  BLOB   NOT NULL,
                     file_size INTEGER NOT NULL,
                    magnet_uri  TEXT   NOT NULL,
                  seeder_count INTEGER NOT NULL,
                 creation_date INTEGER NOT NULL,
                 deletion_date INTEGER,
                 leecher_count INTEGER NOT NULL,
                 user_mod_uuid  BLOB   NOT NULL,
                download_count INTEGER NOT NULL,
             PRIMARY KEY(user_mod_uuid, version),
             FOREIGN KEY(user_mod_uuid) REFERENCES UserMod(uuid)
         ) STRICT",
    )
}

pub fn add(connection: &Connection, user_mod_release: &UserModRelease) -> Result<usize> {
    connection.execute(
        "INSERT INTO UserModRelease(
             creation_date,
             deletion_date,
             download_count,
             file_hash,
             file_size,
             leecher_count,
             magnet_uri,
             seeder_count,
             user_mod_uuid,
             version
         ) VALUES(
             :creation_date,
             :deletion_date,
             :download_count,
             :file_hash,
             :file_size,
             :leecher_count,
             :magnet_uri,
             :seeder_count,
             :user_mod_uuid,
             :version
         )",
        named_params! {
            ":creation_date": user_mod_release.creation_date,
            ":deletion_date": user_mod_release.deletion_date,
            ":download_count": user_mod_release.download_count,
            ":file_hash": user_mod_release.file_hash,
            ":file_size": user_mod_release.file_size,
            ":leecher_count": user_mod_release.leecher_count,
            ":magnet_uri": user_mod_release.magnet_uri,
            ":seeder_count": user_mod_release.seeder_count,
            ":user_mod_uuid": user_mod_release.user_mod_uuid,
            ":version": user_mod_release.version,
        },
    )
}

pub fn delete(
    connection: &Connection,
    user_mod_uuid: &Uuid,
    user_mod_release_deletion_date: u64,
    user_mod_release_version: &Version,
) -> Result<usize> {
    connection
        .prepare(
            "UPDATE UserModRelease
                SET deletion_date = :deletion_date
              WHERE user_mod_uuid = :user_mod_uuid
                AND version = :version",
        )?
        .execute(named_params! {
            ":deletion_date": user_mod_release_deletion_date,
            ":user_mod_uuid": user_mod_uuid,
            ":version": user_mod_release_version
        })
}

pub fn get(
    connection: &Connection,
    user_mod_uuid: &Uuid,
    user_mod_release_version: &Version,
) -> Result<UserModRelease> {
    connection.query_row(
        "SELECT creation_date,
                deletion_date,
                download_count,
                file_hash,
                file_size,
                leecher_count,
                magnet_uri,
                seeder_count,
                user_mod_uuid,
                version
           FROM UserModRelease
          WHERE deletion_date IS NULL
            AND user_mod_uuid = :user_mod_uuid
            AND version = :version",
        named_params! {
            ":user_mod_uuid": user_mod_uuid,
            ":version": user_mod_release_version
        },
        |row| row.try_into(),
    )
}

pub fn get_all(connection: &Connection, user_mod_uuid: &Uuid) -> Result<Vec<UserModRelease>> {
    connection
        .prepare(
            "SELECT creation_date,
                    deletion_date,
                    download_count,
                    file_hash,
                    file_size,
                    leecher_count,
                    magnet_uri,
                    seeder_count,
                    user_mod_uuid,
                    version
               FROM UserModRelease
              WHERE deletion_date IS NULL
                AND user_mod_uuid = :user_mod_uuid",
        )?
        .query_map(named_params! { ":user_mod_uuid": user_mod_uuid }, |row| {
            row.try_into()
        })?
        .collect()
}
