use super::types::password_hash::PasswordHash;
use rusqlite::{named_params, Connection, Result, Row};
use uuid::Uuid;

#[derive(Debug, Eq, PartialEq)]
pub struct User {
    pub creation_date: u64,
    pub password_hash: PasswordHash,
    pub name: String,
    pub uuid: Uuid,
}

impl TryFrom<&Row<'_>> for User {
    type Error = rusqlite::Error;

    fn try_from(row: &Row) -> Result<Self, Self::Error> {
        Ok(User {
            creation_date: row.get("creation_date")?,
            name: row.get("name")?,
            password_hash: row.get("password_hash")?,
            uuid: row.get("uuid")?,
        })
    }
}

pub fn create_table(connection: &Connection) -> Result<()> {
    connection.execute_batch(
        "CREATE TABLE IF NOT EXISTS User(
                      uuid  BLOB   NOT NULL PRIMARY KEY,
                      name  TEXT   NOT NULL UNIQUE,
             creation_date INTEGER NOT NULL,
             password_hash  TEXT   NOT NULL
         ) STRICT",
    )
}

pub fn add(connection: &Connection, user: &User) -> Result<usize> {
    connection.execute(
        "INSERT INTO User(creation_date, password_hash, name, uuid)
         VALUES(:creation_date, :password_hash, :name, :uuid)",
        named_params! {
            ":creation_date": user.creation_date,
            ":name": user.name,
            ":password_hash": user.password_hash,
            ":uuid": user.uuid,
        },
    )
}

pub fn get_all(connection: &Connection) -> Result<Vec<User>> {
    connection
        .prepare(
            "SELECT creation_date, name, password_hash, uuid
               FROM User",
        )?
        .query_map([], |row| row.try_into())?
        .collect()
}

pub fn get_by_name(connection: &Connection, user_name: &str) -> Result<User> {
    connection.query_row(
        "SELECT creation_date, name, password_hash, uuid
           FROM User
          WHERE name = :name",
        named_params! { ":name": user_name },
        |row| row.try_into(),
    )
}

pub fn get_by_uuid(connection: &Connection, user_uuid: &Uuid) -> Result<User> {
    connection.query_row(
        "SELECT creation_date, name, password_hash, uuid
           FROM User
          WHERE uuid = :uuid",
        named_params! { ":uuid": user_uuid },
        |row| row.try_into(),
    )
}

pub fn update_name(connection: &Connection, user_uuid: &Uuid, user_name: &str) -> Result<usize> {
    connection.execute(
        "UPDATE User
            SET name = :name
          WHERE uuid = :uuid",
        named_params! {
            ":name": user_name,
            ":uuid": user_uuid,
        },
    )
}

pub fn update_password(
    connection: &Connection,
    user_uuid: &Uuid,
    user_password_hash: &PasswordHash,
) -> Result<usize> {
    connection.execute(
        "UPDATE User
            SET password_hash = :password_hash
          WHERE uuid = :uuid",
        named_params! {
            ":password_hash": user_password_hash,
            ":uuid": user_uuid,
        },
    )
}
