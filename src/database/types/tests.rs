use super::{hash::Hash, password_hash::PasswordHash, version::Version};
use anyhow::Result;
use argon2::{Argon2, PasswordHasher, PasswordVerifier};
use password_hash::SaltString;
use rand::rngs::OsRng;
use rusqlite::{named_params, Connection};
use uuid::Uuid;

#[test]
pub fn password_hash() -> Result<()> {
    let argon = Argon2::default();
    let password = b"hunter2";
    let salt = SaltString::generate(&mut OsRng);
    let password_hash: PasswordHash = argon.hash_password(password, &salt)?.serialize().into();
    let connection = Connection::open_in_memory()?;
    connection.execute_batch("CREATE TABLE PasswordHash(password_hash TEXT)")?;
    connection.execute(
        "INSERT INTO PasswordHash(password_hash) VALUES(:password_hash)",
        named_params! { ":password_hash": password_hash },
    )?;
    let password_hash_from_sql: PasswordHash = connection.query_row(
        "SELECT password_hash FROM PasswordHash WHERE password_hash = :password_hash",
        named_params! { ":password_hash": password_hash },
        |row| row.get("password_hash"),
    )?;
    let hash = password_hash_from_sql.password_hash();
    argon.verify_password(password, &hash)?;
    Ok(())
}

#[test]
pub fn uuid_hash() -> Result<()> {
    let uuid = Uuid::parse_str("0d1c5d6e-e88e-4a87-9dc5-21251cb70a34")?;
    let uuid_hash: Hash = blake3::hash(uuid.as_bytes()).into();
    let connection = Connection::open_in_memory()?;
    connection.execute_batch("CREATE TABLE UuidHash(uuid_hash BLOB)")?;
    connection.execute(
        "INSERT INTO UuidHash(uuid_hash) VALUES(:uuid_hash)",
        named_params! { ":uuid_hash": uuid_hash },
    )?;
    let uuid_hash_from_sql: Hash = connection.query_row(
        "SELECT uuid_hash FROM UuidHash WHERE uuid_hash = :uuid_hash",
        named_params! { ":uuid_hash": uuid_hash },
        |row| row.get("uuid_hash"),
    )?;
    assert_eq!(uuid_hash_from_sql, uuid_hash);
    Ok(())
}

#[test]
pub fn version() -> Result<()> {
    let version: Version = semver::Version::new(4, 2, 0).into();
    let connection = Connection::open_in_memory()?;
    connection.execute_batch("CREATE TABLE Version(version TEXT)")?;
    connection.execute(
        "INSERT INTO Version(version) VALUES(:version)",
        named_params! { ":version": version },
    )?;
    let version_from_sql: Version = connection.query_row(
        "SELECT version FROM Version WHERE version = :version",
        named_params! { ":version": version },
        |row| row.get("version"),
    )?;
    assert_eq!(version_from_sql, version);
    Ok(())
}
