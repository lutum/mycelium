use password_hash::PasswordHashString;
use rusqlite::types::{FromSql, FromSqlError, FromSqlResult, ToSql, ToSqlOutput, ValueRef};
use rusqlite::Result;
use std::ops::Deref;

#[derive(Debug, Eq, PartialEq)]
pub struct PasswordHash(pub PasswordHashString);

impl Deref for PasswordHash {
    type Target = PasswordHashString;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl From<PasswordHashString> for PasswordHash {
    fn from(hash: PasswordHashString) -> Self {
        PasswordHash(hash)
    }
}

impl FromSql for PasswordHash {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        value.as_str().and_then(|hash_string| {
            PasswordHashString::new(hash_string)
                .map(PasswordHash)
                .map_err(|error| FromSqlError::Other(Box::new(error)))
        })
    }
}

impl ToSql for PasswordHash {
    fn to_sql(&self) -> Result<ToSqlOutput<'_>> {
        Ok(ToSqlOutput::from(self.as_str().to_owned()))
    }
}
