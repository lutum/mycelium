use rusqlite::types::{FromSql, FromSqlError, FromSqlResult, ToSql, ToSqlOutput, ValueRef};
use rusqlite::Result;
use std::fmt::{self, Display};
use std::ops::Deref;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Hash(pub blake3::Hash);

impl Deref for Hash {
    type Target = blake3::Hash;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Display for Hash {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl From<blake3::Hash> for Hash {
    fn from(hash: blake3::Hash) -> Self {
        Hash(hash)
    }
}

impl FromSql for Hash {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        value.as_blob().and_then(|blob| {
            let hash: [u8; 32] = blob
                .try_into()
                .map_err(|error| FromSqlError::Other(Box::new(error)))?;
            Ok(blake3::Hash::from(hash).into())
        })
    }
}

impl ToSql for Hash {
    fn to_sql(&self) -> Result<ToSqlOutput<'_>> {
        Ok(ToSqlOutput::from(self.as_bytes() as &[u8]))
    }
}
