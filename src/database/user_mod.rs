use rusqlite::{named_params, Connection, Result, Row};
use uuid::Uuid;

#[derive(Debug, Eq, PartialEq)]
pub struct UserMod {
    pub creation_date: u64,
    pub description: Option<String>,
    pub name: String,
    pub user_uuid: Uuid,
    pub uuid: Uuid,
}

impl TryFrom<&Row<'_>> for UserMod {
    type Error = rusqlite::Error;

    fn try_from(row: &Row) -> Result<Self, Self::Error> {
        Ok(UserMod {
            creation_date: row.get("creation_date")?,
            description: row.get("description")?,
            name: row.get("name")?,
            user_uuid: row.get("user_uuid")?,
            uuid: row.get("uuid")?,
        })
    }
}

pub fn create_table(connection: &Connection) -> Result<()> {
    connection.execute_batch(
        "BEGIN;
         CREATE TABLE IF NOT EXISTS UserMod(
                      uuid  BLOB   NOT NULL PRIMARY KEY,
                      name  TEXT   NOT NULL,
                 user_uuid  BLOB   NOT NULL,
               description  TEXT,
             creation_date INTEGER NOT NULL,
             FOREIGN KEY(user_uuid) REFERENCES User(uuid)
         ) STRICT;
         CREATE VIRTUAL TABLE IF NOT EXISTS UserModFts USING fts5(name, uuid UNINDEXED, tokenize = 'trigram');
         CREATE TRIGGER IF NOT EXISTS UserModAfterInsert AFTER INSERT ON UserMod BEGIN
             INSERT INTO UserModFts(name, uuid) VALUES(new.name, new.uuid);
         END;
         CREATE TRIGGER IF NOT EXISTS UserModAfterUpdate AFTER UPDATE OF name ON UserMod BEGIN
             UPDATE UserModFts SET name = new.name WHERE uuid = new.uuid;
         END;
         CREATE TRIGGER IF NOT EXISTS UserModAfterDelete AFTER DELETE ON UserMod BEGIN
             DELETE FROM UserModFts WHERE uuid = old.uuid;
         END;
         COMMIT;",
    )
}

pub fn add(connection: &Connection, user_mod: &UserMod) -> Result<usize> {
    connection.execute(
        "INSERT INTO UserMod(creation_date, description, name, user_uuid, uuid)
         VALUES(:creation_date, :description, :name, :user_uuid, :uuid)",
        named_params! {
            ":creation_date": user_mod.creation_date,
            ":description": user_mod.description,
            ":name": user_mod.name,
            ":user_uuid": user_mod.user_uuid,
            ":uuid": user_mod.uuid,
        },
    )
}

pub fn get(connection: &Connection, user_mod_uuid: &Uuid) -> Result<UserMod> {
    connection.query_row(
        "SELECT creation_date, description, name, user_uuid, uuid
           FROM UserMod
          WHERE uuid = :uuid",
        named_params! { ":uuid": user_mod_uuid },
        |row| row.try_into(),
    )
}

pub fn get_all(connection: &Connection) -> Result<Vec<UserMod>> {
    connection
        .prepare(
            "SELECT creation_date, description, name, user_uuid, uuid
               FROM UserMod",
        )?
        .query_map([], |row| row.try_into())?
        .collect()
}

pub fn get_latest(connection: &Connection, count: u64, offset: u64) -> Result<Vec<UserMod>> {
    connection
        .prepare(
            "  SELECT creation_date, description, name, user_uuid, uuid
                 FROM UserMod
             ORDER BY creation_date DESC
                LIMIT :count
               OFFSET :offset",
        )?
        .query_map(
            named_params! {
                ":count": count,
                ":offset": offset,
            },
            |row| row.try_into(),
        )?
        .collect()
}

pub fn search(
    connection: &Connection,
    count: u64,
    offset: u64,
    name: &str,
) -> Result<Vec<UserMod>> {
    connection
        .prepare(
            "  SELECT name, uuid
                 FROM UserModFts(:name)
             ORDER BY rank
                LIMIT :count
               OFFSET :offset",
        )?
        .query_map(
            named_params! {
                ":count": count,
                ":name": name,
                ":offset": offset,
            },
            |row| {
                let uuid: Uuid = row.get("uuid")?;
                get(connection, &uuid)
            },
        )?
        .collect()
}

pub fn search_by_user_uuid(
    connection: &Connection,
    count: u64,
    offset: u64,
    user_uuid: &Uuid,
) -> Result<Vec<UserMod>> {
    connection
        .prepare(
            "SELECT creation_date, description, name, user_uuid, uuid
               FROM UserMod
              WHERE user_uuid = :user_uuid
              LIMIT :count
             OFFSET :offset",
        )?
        .query_map(
            named_params! {
                ":count": count,
                ":offset": offset,
                ":user_uuid": user_uuid,
            },
            |row| row.try_into(),
        )?
        .collect()
}

pub fn count(connection: &Connection) -> Result<u64> {
    connection.query_row("SELECT COUNT(*) FROM UserMod", [], |row| row.get(0))
}

pub fn count_by_search(connection: &Connection, name: &String) -> Result<u64> {
    connection.query_row(
        "SELECT COUNT(*) FROM UserModFts(:name)",
        named_params! { ":name": name },
        |row| row.get(0),
    )
}

pub fn count_by_user_uuid(connection: &Connection, user_uuid: &Uuid) -> Result<u64> {
    connection.query_row(
        "SELECT COUNT(*) FROM UserMod WHERE user_uuid = :user_uuid",
        named_params! { ":user_uuid": user_uuid },
        |row| row.get(0),
    )
}

pub fn update_description(
    connection: &Connection,
    user_mod_uuid: &Uuid,
    description: Option<&str>,
) -> Result<usize> {
    connection.execute(
        "UPDATE UserMod
            SET description = :description
          WHERE uuid = :uuid",
        named_params! {
            ":description": description,
            ":uuid": user_mod_uuid,
        },
    )
}

pub fn update_name(
    connection: &Connection,
    user_mod_uuid: &Uuid,
    user_mod_name: &str,
) -> Result<usize> {
    connection.execute(
        "UPDATE UserMod
            SET name = :name
          WHERE uuid = :uuid",
        named_params! {
            ":name": user_mod_name,
            ":uuid": user_mod_uuid,
        },
    )
}
