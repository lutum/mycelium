use rusqlite::{named_params, Connection, Result, Row};
use uuid::Uuid;

pub struct UserModImage {
    pub file_name: String,
    pub position: u64,
    pub user_mod_uuid: Uuid,
}

impl TryFrom<&Row<'_>> for UserModImage {
    type Error = rusqlite::Error;

    fn try_from(row: &Row) -> Result<Self, Self::Error> {
        Ok(UserModImage {
            file_name: row.get("file_name")?,
            position: row.get("position")?,
            user_mod_uuid: row.get("user_mod_uuid")?,
        })
    }
}

pub fn create_table(connection: &Connection) -> Result<()> {
    connection.execute_batch(
        "CREATE TABLE IF NOT EXISTS UserModImage(
                 file_name  TEXT   NOT NULL PRIMARY KEY,
                  position INTEGER NOT NULL,
             user_mod_uuid  BLOB   NOT NULL,
             FOREIGN KEY(user_mod_uuid) REFERENCES UserMod(uuid)
         ) STRICT",
    )
}

pub fn add(connection: &Connection, user_mod_image: &UserModImage) -> Result<usize> {
    connection.execute(
        "INSERT INTO UserModImage(file_name, position, user_mod_uuid)
         VALUES(:file_name, :position, :user_mod_uuid)",
        named_params! {
            ":file_name": user_mod_image.file_name,
            ":position": user_mod_image.position,
            ":user_mod_uuid": user_mod_image.user_mod_uuid,
        },
    )
}

pub fn count(connection: &Connection) -> Result<u64> {
    connection.query_row("SELECT COUNT(*) FROM UserModImage", [], |row| row.get(0))
}

pub fn delete(connection: &Connection, user_mod_uuid: &Uuid, position: u64) -> Result<u64> {
    connection.query_row(
        "DELETE
           FROM UserModImage
          WHERE position = :position
            AND user_mod_uuid = :user_mod_uuid",
        named_params! {
            ":position": position,
            ":user_mod_uuid": user_mod_uuid,
        },
        |row| row.get(0),
    )
}

pub fn get(connection: &Connection, user_mod_uuid: &Uuid, position: u64) -> Result<UserModImage> {
    connection.query_row(
        "SELECT file_name, position, user_mod_uuid
           FROM UserModImage
          WHERE position = :position
            AND user_mod_uuid = :user_mod_uuid",
        named_params! {
            ":position": position,
            ":user_mod_uuid": user_mod_uuid,
        },
        |row| row.try_into(),
    )
}

pub fn get_all(connection: &Connection, user_mod_uuid: &Uuid) -> Result<Vec<UserModImage>> {
    connection
        .prepare(
            "  SELECT file_name, position, user_mod_uuid
                 FROM UserModImage
                WHERE user_mod_uuid = :user_mod_uuid
             ORDER BY position ASC",
        )?
        .query_map(named_params! { ":user_mod_uuid": user_mod_uuid }, |row| {
            row.try_into()
        })?
        .collect()
}
