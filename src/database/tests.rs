use super::user::{self, User};
use super::user_mod::{self, UserMod};
use super::user_session::{self, UserSession};
use crate::database::create_tables;
use anyhow::Result;
use argon2::{Argon2, PasswordHasher};
use password_hash::SaltString;
use paste::paste;
use rand::rngs::OsRng;
use rusqlite::{named_params, Connection};
use uuid::Uuid;

macro_rules! test_create_table {
    ($identifier: ident, $table_name: expr) => {
        paste! {
            #[test]
            pub fn [<create_ $identifier _table>]() -> Result<()> {
                let connection = Connection::open_in_memory()?;
                super::[<$identifier>]::create_table(&connection)?;
                let table_count: u64 = connection
                    .query_row(
                        "SELECT COUNT(*)
                           FROM sqlite_master
                          WHERE type = 'table'
                            AND name = :table_name",
                        named_params! { ":table_name": $table_name, },
                        |row| row.get(0),
                    )?;
                assert_eq!(table_count, 1);
                Ok(())
            }
        }
    };
}

test_create_table!(user_mod_image, "UserModImage");
test_create_table!(user_mod_release, "UserModRelease");
test_create_table!(user_mod, "UserMod");
test_create_table!(user_session, "UserSession");
test_create_table!(user, "User");

macro_rules! password_hash {
    ($password: expr) => {
        Argon2::default()
            .hash_password($password, &SaltString::generate(&mut OsRng))?
            .serialize()
            .into()
    };
}

macro_rules! user {
    () => {
        User {
            creation_date: 441759600,
            name: "AzureDiamond".to_string(),
            password_hash: password_hash!(b"hunter2"),
            uuid: Uuid::parse_str("0d1c5d6e-e88e-4a87-9dc5-21251cb70a34")?,
        }
    };
}

macro_rules! user_mod {
    ($user: expr) => {
        UserMod {
            creation_date: 441759600,
            description: Some("An artifact in Greek mythology.".to_string()),
            name: "Pandora's box".to_string(),
            user_uuid: $user.uuid,
            uuid: Uuid::parse_str("10fa9ea8-d421-427c-9016-8c6f68cf9f4e")?,
        }
    };
}

macro_rules! user_session {
    ($user: expr) => {
        UserSession {
            expiration_date: 441759600,
            user_uuid: $user.uuid,
            uuid_hash: blake3::hash(
                Uuid::parse_str("79d65918-9b60-4cf4-aba5-ff621f8daf90")?.as_bytes(),
            )
            .into(),
        }
    };
}

//////////
// User //
//////////

#[test]
pub fn add_user() -> Result<()> {
    let connection = Connection::open_in_memory()?;
    create_tables(&connection)?;
    let user = user!();
    let added_row_count = user::add(&connection, &user)?;
    assert_eq!(added_row_count, 1);
    Ok(())
}

#[test]
pub fn get_user_by_name() -> Result<()> {
    let connection = Connection::open_in_memory()?;
    create_tables(&connection)?;
    let user = user!();
    user::add(&connection, &user)?;
    let user_from_sql = user::get_by_name(&connection, &user.name)?;
    assert_eq!(user_from_sql, user);
    Ok(())
}

#[test]
pub fn get_user_by_uuid() -> Result<()> {
    let connection = Connection::open_in_memory()?;
    create_tables(&connection)?;
    let user = user!();
    user::add(&connection, &user)?;
    let user_from_sql = user::get_by_uuid(&connection, &user.uuid)?;
    assert_eq!(user_from_sql, user);
    Ok(())
}

#[test]
pub fn update_user_name() -> Result<()> {
    let connection = Connection::open_in_memory()?;
    create_tables(&connection)?;
    let user = user!();
    user::add(&connection, &user)?;
    let name = "Cthon98";
    user::update_name(&connection, &user.uuid, name)?;
    let user_from_sql = user::get_by_uuid(&connection, &user.uuid)?;
    assert_eq!(user_from_sql.name, name);
    Ok(())
}

#[test]
pub fn update_user_password() -> Result<()> {
    let connection = Connection::open_in_memory()?;
    create_tables(&connection)?;
    let user = user!();
    user::add(&connection, &user)?;
    let password_hash = password_hash!(b"trustno1");
    user::update_password(&connection, &user.uuid, &password_hash)?;
    let user_from_sql = user::get_by_uuid(&connection, &user.uuid)?;
    assert_eq!(user_from_sql.password_hash, password_hash);
    Ok(())
}

//////////////
// User mod //
//////////////

#[test]
pub fn add_user_mod() -> Result<()> {
    let connection = Connection::open_in_memory()?;
    create_tables(&connection)?;
    let user = user!();
    user::add(&connection, &user)?;
    let user_mod = user_mod!(user);
    let added_row_count = user_mod::add(&connection, &user_mod)?;
    assert_eq!(added_row_count, 1);
    Ok(())
}

#[test]
pub fn delete_user_mod() -> Result<()> {
    unimplemented!();
}

#[test]
pub fn get_user_mod() -> Result<()> {
    let connection = Connection::open_in_memory()?;
    create_tables(&connection)?;
    let user = user!();
    user::add(&connection, &user)?;
    let user_mod = user_mod!(user);
    user_mod::add(&connection, &user_mod)?;
    let user_mod_from_sql = user_mod::get(&connection, &user_mod.uuid)?;
    assert_eq!(user_mod_from_sql, user_mod);
    Ok(())
}

#[test]
pub fn update_user_mod_description() -> Result<()> {
    let connection = Connection::open_in_memory()?;
    create_tables(&connection)?;
    let user = user!();
    user::add(&connection, &user)?;
    let user_mod = user_mod!(user);
    user_mod::add(&connection, &user_mod)?;
    let description = Some("Mítico recipiente de la mitología griega.");
    user_mod::update_description(&connection, &user_mod.uuid, description)?;
    let user_mod_from_sql = user_mod::get(&connection, &user_mod.uuid)?;
    assert_eq!(user_mod_from_sql.description.as_deref(), description);
    Ok(())
}

#[test]
pub fn update_user_mod_name() -> Result<()> {
    let connection = Connection::open_in_memory()?;
    create_tables(&connection)?;
    let user = user!();
    user::add(&connection, &user)?;
    let user_mod = user_mod!(user);
    user_mod::add(&connection, &user_mod)?;
    let name = "Caja de Pandora";
    user_mod::update_name(&connection, &user_mod.uuid, name)?;
    let user_mod_from_sql = user_mod::get(&connection, &user_mod.uuid)?;
    assert_eq!(user_mod_from_sql.name, name);
    Ok(())
}

////////////////////
// User mod image //
////////////////////

#[test]
pub fn add_user_mod_image() -> Result<()> {
    unimplemented!();
}

#[test]
pub fn delete_user_mod_image() -> Result<()> {
    unimplemented!();
}

#[test]
pub fn get_user_mod_image() -> Result<()> {
    unimplemented!();
}

//////////////////////
// User mod release //
//////////////////////

#[test]
pub fn add_user_mod_release() -> Result<()> {
    unimplemented!();
}

#[test]
pub fn delete_user_mod_release() -> Result<()> {
    unimplemented!();
}

#[test]
pub fn get_user_mod_release() -> Result<()> {
    unimplemented!();
}

//////////////////
// User session //
//////////////////

#[test]
pub fn add_user_session() -> Result<()> {
    let connection = Connection::open_in_memory()?;
    create_tables(&connection)?;
    let user = user!();
    user::add(&connection, &user)?;
    let user_session = user_session!(user);
    let added_row_count = user_session::add(&connection, &user_session)?;
    assert_eq!(added_row_count, 1);
    Ok(())
}

#[test]
pub fn delete_user_session() -> Result<()> {
    let connection = Connection::open_in_memory()?;
    create_tables(&connection)?;
    let user = user!();
    user::add(&connection, &user)?;
    let user_session = user_session!(user);
    user_session::add(&connection, &user_session)?;
    let deleted_row_count = user_session::delete(&connection, &user_session.uuid_hash)?;
    assert_eq!(deleted_row_count, 1);
    Ok(())
}

#[test]
pub fn get_user_session() -> Result<()> {
    let connection = Connection::open_in_memory()?;
    create_tables(&connection)?;
    let user = user!();
    user::add(&connection, &user)?;
    let user_session = user_session!(user);
    user_session::add(&connection, &user_session)?;
    let user_session_from_sql = user_session::get(&connection, &user_session.uuid_hash)?;
    assert_eq!(user_session_from_sql, user_session);
    Ok(())
}
