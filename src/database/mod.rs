use anyhow::Result;
use rusqlite::Connection;

pub mod types;
pub mod user;
pub mod user_mod;
pub mod user_mod_image;
pub mod user_mod_release;
pub mod user_session;

#[cfg(test)]
mod tests;

pub fn create_tables(connection: &Connection) -> Result<()> {
    user::create_table(connection)?;
    user_mod::create_table(connection)?;
    user_mod_image::create_table(connection)?;
    user_mod_release::create_table(connection)?;
    user_session::create_table(connection)?;
    Ok(())
}
