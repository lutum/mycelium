use crate::constants::{OPENTRACKER_ANNOUNCE_URL, TORRENT_PIECE_LENGTH};
use anyhow::Result;
use axum::body::StreamBody;
use axum::headers::HeaderName;
use axum::http::header::{self, HeaderValue};
use axum::response::{IntoResponse, Response};
use serde::{Deserialize, Serialize};
use serde_bytes::ByteBuf;
use sha1::{Digest, Sha1};
use std::path::{Path, PathBuf};
use tokio::fs::File;
use tokio_util::io::ReaderStream;

static CONTENT_TYPE_HEADER: (HeaderName, HeaderValue) = (
    header::CONTENT_TYPE,
    HeaderValue::from_static("application/x-bittorrent"),
);

// References:
// - https://www.bittorrent.org/beps/bep_0003.html#info-dictionary
// - https://wiki.theory.org/BitTorrentSpecification#Info_Dictionary
#[derive(Debug, Deserialize, Serialize)]
pub struct Info {
    pub name: String,
    pub pieces: ByteBuf,
    #[serde(rename = "piece length")]
    pub piece_length: u64,
    pub length: u64,
}

impl Info {
    pub fn hash_string(&self) -> String {
        let bytes = serde_bencode::to_bytes(&self).unwrap();
        let mut hasher = Sha1::new();
        hasher.update(&bytes);
        let hash = hasher.finalize();
        format!("{:02x}", hash)
    }
}

// References:
// - https://www.bittorrent.org/beps/bep_0003.html#metainfo-files
// - https://wiki.theory.org/BitTorrentSpecification#Metainfo_File_Structure
#[derive(Debug, Deserialize, Serialize)]
pub struct Torrent {
    pub announce: String,
    pub info: Info,
}

impl Torrent {
    pub fn new(file_name: &str, file_bytes: &[u8]) -> Torrent {
        let pieces: Vec<u8> = file_bytes
            .chunks(TORRENT_PIECE_LENGTH as usize)
            .flat_map(|piece| {
                let mut hasher = Sha1::new();
                hasher.update(piece);
                hasher.finalize()
            })
            .collect();

        Torrent {
            announce: OPENTRACKER_ANNOUNCE_URL.to_owned(),
            info: Info {
                name: file_name.to_owned(),
                pieces: ByteBuf::from(pieces),
                piece_length: TORRENT_PIECE_LENGTH,
                length: file_bytes.len() as u64,
            },
        }
    }

    // Reference: https://www.bittorrent.org/beps/bep_0009.html#magnet-uri-format
    pub fn magnet_uri(&self) -> String {
        format!(
            "magnet:?xt=urn:btih:{}&tr={}",
            self.info.hash_string(),
            urlencoding::encode(&self.announce)
        )
    }
}

pub struct TorrentFile {
    pub file_name: String,
    pub file: File,
}

impl TorrentFile {
    pub async fn open(file_path: &Path) -> Result<TorrentFile> {
        let path = PathBuf::from(file_path);
        let file_name = path
            .file_name()
            .expect("Torrent path should have a file name")
            .to_string_lossy()
            .to_string();
        let file = File::open(&path).await?;
        Ok(TorrentFile { file_name, file })
    }

    pub fn response_headers(&self) -> [(HeaderName, HeaderValue); 2] {
        let content_disposition_string = format!("attachment; filename=\"{}\"", self.file_name);
        let content_disposition_value = HeaderValue::from_str(&content_disposition_string)
            .expect("Content disposition string should be valid");
        let content_disposition_header = (header::CONTENT_DISPOSITION, content_disposition_value);
        [content_disposition_header, CONTENT_TYPE_HEADER.clone()]
    }
}

impl IntoResponse for TorrentFile {
    fn into_response(self) -> Response {
        let headers = self.response_headers();
        let stream = ReaderStream::new(self.file);
        let body = StreamBody::new(stream);
        (headers, body).into_response()
    }
}
