pub const DATABASE_FILE_NAME: &str = "database.sqlite3";

pub const MAX_USER_NAME_LENGTH: u64 = 64;
pub const MIN_USER_NAME_LENGTH: u64 = 1;

pub const MAX_USER_PASSWORD_LENGTH: u64 = 64;
pub const MIN_USER_PASSWORD_LENGTH: u64 = 8;

pub const MAX_USER_MOD_DESCRIPTION_LENGTH: u64 = 4096;

pub const MAX_USER_MOD_NAME_LENGTH: u64 = 64;
pub const MIN_USER_MOD_NAME_LENGTH: u64 = 1;

pub const USER_SESSION_DURATION: u64 = 60 * 60 * 24;

pub const IMAGES_FOLDER_PATH: &str = "./images";

pub const OPENTRACKER_ANNOUNCE_URL: &str = "http://127.0.0.1:6969/announce";
pub const OPENTRACKER_WHITELIST_PATH: &str = "./opentracker/whitelist";

pub const TORRENT_PIECE_LENGTH: u64 = bytesize::KB * 512;
pub const TORRENTS_FOLDER_PATH: &str = "./torrents";

pub const TRANSMISSION_DOWNLOADS_FOLDER_PATH: &str = "./transmission/downloads/complete";
pub const TRANSMISSION_WATCH_FOLDER_PATH: &str = "./transmission/watch";
