use axum::response::{IntoResponse, Response};
use hyper::StatusCode;
use thiserror::Error;
use tracing::error;

#[derive(Debug, Error)]
pub enum ServerError {
    #[error(transparent)]
    ConnectionPool(#[from] r2d2::Error),
    #[error(transparent)]
    Form(#[from] axum::extract::rejection::FormRejection),
    #[error(transparent)]
    Generic(#[from] anyhow::Error),
    #[error(transparent)]
    Image(#[from] image::ImageError),
    #[error(transparent)]
    Io(#[from] std::io::Error),
    #[error(transparent)]
    PasswordHash(#[from] password_hash::Error),
    #[error(transparent)]
    SerdeBencode(#[from] serde_bencode::Error),
    #[error(transparent)]
    Sqlite(#[from] rusqlite::Error),
    #[error(transparent)]
    Uuid(#[from] uuid::Error),
    #[error(transparent)]
    Validation(#[from] validator::ValidationErrors),
}

impl IntoResponse for ServerError {
    fn into_response(self) -> Response {
        error!(error = %self);
        (StatusCode::INTERNAL_SERVER_ERROR).into_response()
    }
}
