use axum::{extract::State, http::Request, middleware::Next, response::Response};
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;

use crate::error::ServerError;

pub async fn connection_pool<B>(
    State(connection_pool): State<Pool<SqliteConnectionManager>>,
    mut request: Request<B>,
    next: Next<B>,
) -> Result<Response, ServerError> {
    request.extensions_mut().insert(connection_pool);
    let response = next.run(request).await;
    Ok(response)
}
