use crate::constants::OPENTRACKER_WHITELIST_PATH;
use crate::torrent::Torrent;
use anyhow::Result;
use tokio::fs::{self, OpenOptions};
use tokio::io::AsyncWriteExt;

pub async fn add(torrent: &Torrent) -> Result<()> {
    let mut file = OpenOptions::new()
        .append(true)
        .open(OPENTRACKER_WHITELIST_PATH)
        .await?;
    let line = format!("{}\n", &torrent.info.hash_string());
    file.write_all(line.as_bytes()).await?;
    Ok(())
}

async fn delete(torrent: &Torrent) -> Result<()> {
    let hash_to_delete = torrent.info.hash_string();
    let hashes = torrent_hashes()
        .await?
        .iter()
        .skip_while(|hash| **hash == hash_to_delete)
        .map(|hash| format!("{}\n", hash))
        .collect::<Vec<String>>()
        .join("");
    fs::write(OPENTRACKER_WHITELIST_PATH, hashes).await?;
    Ok(())
}

async fn torrent_hashes() -> Result<Vec<String>> {
    Ok(fs::read_to_string(OPENTRACKER_WHITELIST_PATH)
        .await?
        .split('\n')
        .map(|line| line.to_string())
        .collect())
}
