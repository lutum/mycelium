use std::path::PathBuf;

use anyhow::Result;
use argon2::Argon2;
use axum::routing::{delete, get, patch};
use axum::{Extension, Router};
use constants::DATABASE_FILE_NAME;
use lazy_static::lazy_static;
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;
use tower_http::trace::TraceLayer;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

mod constants;
mod database;
mod error;
mod route;
mod torrent;
mod tracker;
mod transmission;

#[macro_export]
macro_rules! now {
    () => {
        std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .expect("Failed to get seconds since UNIX epoch")
            .as_secs()
    };
}

lazy_static! {
    pub static ref ARGON2: Argon2<'static> = Argon2::default();
}

#[tokio::main]
async fn main() {
    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(
            std::env::var("RUST_LOG").unwrap_or_else(|_| "mycelium=debug,tower_http=debug".into()),
        ))
        .with(
            tracing_subscriber::fmt::layer()
                .with_file(true)
                .with_line_number(true),
        )
        .init();

    let database_file = PathBuf::from(DATABASE_FILE_NAME);
    let connection_manager = SqliteConnectionManager::file(database_file)
        .with_init(|connection| connection.pragma_update(None, "journal_mode", "WAL"));
    let connection_pool =
        r2d2::Pool::new(connection_manager).expect("Failed to create database connection pool");

    let router = router(connection_pool).expect("Failed to create router");

    // TODO: Add graceful shutdown.
    axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
        .serve(router.into_make_service())
        .await
        .unwrap();
}

pub fn router(connection_pool: Pool<SqliteConnectionManager>) -> Result<Router> {
    let connection = connection_pool.get()?;
    database::create_tables(&connection)?;

    let users_router = Router::new().nest(
        "/users",
        Router::new()
            .route(
                "/",
                get(route::users::list_users).post(route::users::create_user),
            )
            .nest(
                "/:uuid",
                Router::new()
                    .route("/", get(route::users::get_user))
                    .route("/name", patch(route::users::update_user_name))
                    .route("/password", patch(route::users::update_user_password))
                    .nest(
                        "/sessions",
                        Router::new()
                            .route(
                                "/",
                                get(route::user_sessions::list_user_sessions)
                                    .post(route::user_sessions::create_user_session),
                            )
                            .route(
                                "/:uuid",
                                delete(route::user_sessions::delete_user_session)
                                    .get(route::user_sessions::get_user_session),
                            ),
                    ),
            ),
    );

    let user_mod_releases_router = Router::new().nest(
        "/releases",
        Router::new()
            .route(
                "/",
                get(route::user_mod_releases::list_user_mod_releases)
                    .post(route::user_mod_releases::create_user_mod_release),
            )
            .nest(
                "/:version",
                Router::new()
                    .route(
                        "/",
                        delete(route::user_mod_releases::delete_user_mod_release)
                            .get(route::user_mod_releases::get_user_mod_release),
                    )
                    .route(
                        "/torrent",
                        get(route::user_mod_releases::get_user_mod_release_torrent),
                    ),
            ),
    );

    let user_mods_router = Router::new().nest(
        "/user-mods",
        Router::new()
            .route(
                "/",
                get(route::user_mods::list_user_mods).post(route::user_mods::create_user_mod),
            )
            .nest(
                "/:uuid",
                Router::new()
                    .route("/", get(route::user_mods::get_user_mod))
                    .route("/name", patch(route::user_mods::update_user_mod_name))
                    .route(
                        "/description",
                        patch(route::user_mods::update_user_mod_description),
                    )
                    .nest(
                        "/images",
                        Router::new()
                            .route(
                                "/",
                                get(route::user_mod_images::list_user_mod_images)
                                    .post(route::user_mod_images::create_user_mod_image),
                            )
                            .route(
                                "/:position",
                                get(route::user_mod_images::get_user_mod_image),
                            ),
                    )
                    .merge(user_mod_releases_router),
            ),
    );

    Ok(Router::new()
        .merge(users_router)
        .merge(user_mods_router)
        .layer(Extension(connection_pool))
        .layer(TraceLayer::new_for_http()))
}
