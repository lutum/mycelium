<div align="center">
  <img width="128" src="./icon.svg">
  <h1>Mycelium</h1>
  A game mod sharing platform backend
</div>

## Getting Started

1. Clone this repository:

```
git clone https://gitlab.com/lutum/mycelium.git
```

2. Go inside the project's folder:

```
cd mycelium
```

3. Start the BitTorrent client and tracker:

```
docker-compose up
```

4. Start the server:

```
cargo run
```

The server should now be reachable at [localhost:3000](http://localhost:3000/).

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
